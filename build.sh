#!/bin/bash

echo start dbus
mkdir -p /var/run/dbus
dbus-daemon --system

echo start pulseaudio
usermod -a -G pulse-access root
pulseaudio --system -D

echo start maven build
#while true; do sleep 1; done
mvn clean install
