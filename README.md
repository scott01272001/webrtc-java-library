This project uses [webrtc-java](https://github.com/devopvoid/webrtc-java) to build an flexible library for Java8.

------
## Build

### Required software

  * Docker
  
### Build
    
    1. First build the docker image
    
       docker build -t webrtc -f ${platform}.dockerfile .
    
    2. Build inside docker
    
       docker run -ti --rm -v $(pwd):/root/workspace -w /root/workspace webrtc ./build.sh
       
------

## Environment Required

### Linux:
    * libstdc++6.so support GLIBCXX3.26.0 or later versions.
    * java8 and later versions.
    * if in docker environment:
        need to start d-bus-daemon and pulseaudio server (ref:https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/SystemWide/)