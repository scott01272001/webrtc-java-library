def platforms = ['linux-x86_64']

properties([
     disableConcurrentBuilds(),
     buildDiscarder(logRotator(numToKeepStr: '5')),
     parameters([
        booleanParam(name: 'RELEASE', defaultValue: false, description: 'Release deb to private apt repository')
     ])
 ])
 
def builds = [:]
for(p in platforms) {
    
    def platform = p
    builds[platform] = {
        stage("${platform.capitalize()}: Build/Test") {
        
            def image = docker.build("webrtc/${platform}", "-f ${platform}.dockerfile .")
            image.inside("-u root") {
                checkout scm

                sh "chmod +x -R ${env.WORKSPACE}"
                sh "./build.sh"

                archiveArtifacts "iy-webrtc-java/target/${platform}/*.jar"
            }
        }
    }
}
 
node {
    try {
        checkout scm
        
        parallel builds
        
        stage('Nexus Snapshot') {
           withEnv(["PATH+MAVEN=${tool 'Maven 3.3.9'}/bin"]) {
               configFileProvider([configFile(fileId: 'nexus-deploy', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                   version = sh(returnStdout: true, script: 'ls iy-webrtc-java/target/linux-x86_64/*-linux*.jar | sed -r "s/.*iy-webrtc-java-([0-9a-zA-Z\\.-]+)-linux.*/\\1/g"').trim()
                   url = (version =~ 'SNAPSHOT') ? 'maven-snapshots' : 'maven-releases'
                   repositoryId = (version =~ 'SNAPSHOT') ? 'nexus-snapshots' : 'nexus-releases'

                   sh """mvn -B -gs $MAVEN_GLOBAL_SETTINGS -pl iy-webrtc-java org.apache.maven.plugins:maven-deploy-plugin:3.0.0-M1:deploy-file \
                   -Durl=http://172.16.15.10:8081/repository/${url} \
                   -DrepositoryId=${repositoryId} \
                   -DpomFile=pom.xml \
                   -Dfile=target/linux-x86_64/iy-webrtc-java-${version}.jar \
                   -Dsources=target/linux-x86_64/iy-webrtc-java-${version}-sources.jar \
                   -Dfiles=iy-webrtc-jni/target/iy-webrtc-java-${version}-linux-x86_64.jar \
                   -Dtypes=jar \
                   -Dclassifiers=linux-x86_64 \
                   """
               }
            }
        }
    }catch(e) {
       emailext attachLog: true, 
                subject: "Jenkins Build FAILURE: Job ${env.JOB_NAME}", 
                body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}", 
                recipientProviders: [developers(), requestor()]
       throw e
    }
}

import org.jenkinsci.plugins.pipeline.modeldefinition.Utils

def stage(name, execute, block) {
    return stage(name, execute ? block : {
        echo "Stage \"$name\" skipped."
        Utils.markStageSkippedForConditional(STAGE_NAME)
    })
}
