FROM ubuntu:18.04

RUN apt-get update && apt-get install -y software-properties-common \
    && apt-get update && apt-get install -y --no-install-recommends \
    && add-apt-repository ppa:ubuntu-toolchain-r/test
    
RUN apt-get update && apt-get --only-upgrade install libstdc++6 -y

RUN apt-get install -y --no-install-recommends \
        openjdk-8-jdk \
        build-essential \
        wget \
        git \
        libpulse-dev \
        libv4l-dev \
        libudev-dev \
        libasound2-dev \
        libx11-dev \
    && rm -rf /var/lib/apt/lists/*
    
RUN apt update && apt install -y \
    pulseaudio \
    python3-pip

ARG MAVEN_VERSION=3.6.3
ARG MAVEN_HOME=/usr/share/maven

RUN wget --quiet http://ftp.mirror.tw/pub/apache/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
    && mkdir -p ${MAVEN_HOME} \
    && tar -xzf apache-maven-${MAVEN_VERSION}-bin.tar.gz -C ${MAVEN_HOME} --strip-components=1 \
    && rm -rf apache-maven-${MAVEN_VERSION}-bin.tar.gz \
    && ln -s ${MAVEN_HOME}/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME ${MAVEN_HOME}
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

CMD ["/bin/bash"]